using UnityEngine;
using DG.Tweening;

public class BallColor : MonoBehaviour {
    private new MeshRenderer renderer;
    public Material mat;

    public bool changeColorOneTime = true;
    public enum StartColor { startColor0, startColor1, startColor2, startColor3 };
    public StartColor startColor;
    [SerializeField] 
    [Range(0.05f, 1f)] 
    private float colorFadeDuration = 0.25f;

    public Color color0;
    public Color color1;
    public Color color2;
    public Color color3;

    public Color currentColor;

    public int totalColors = 3;
    public int collisionCounter;

    private void Awake() => DOTween.Init(true, true, LogBehaviour.Default);

    private void Start() {
        renderer = transform.GetComponent<MeshRenderer>();
        mat = renderer.material;

        switch (startColor) {
            case StartColor.startColor0:
                currentColor = color0;
                collisionCounter = 0;
                break;
            case StartColor.startColor1:
                currentColor = color1;
                collisionCounter = 1;
                break;
            case StartColor.startColor2:
                currentColor = color2;
                collisionCounter = 2;
                break;
            case StartColor.startColor3:
                currentColor = color3;
                collisionCounter = 3;
                break;
        }

        mat.color = currentColor;
    }

    private void OnCollisionEnter(Collision collision) {
        if (!collision.transform.CompareTag("Ball")) 
            return;
        Debug.Log("Hit a ball!");

        collisionCounter++;
        collisionCounter %= totalColors;

        Debug.Log("Collision counter is " + collisionCounter);

        if (!changeColorOneTime) {
            switch (collisionCounter) {
                case 0:
                    currentColor = color0;
                    break;
                case 1:
                    currentColor = color1;
                    break;
                case 2:
                    currentColor = color2;
                    break;
                case 3:
                    currentColor = color3;
                    break;
            }
        }
        else
            currentColor = color1;

        mat.DOColor(currentColor, colorFadeDuration).SetEase(Ease.OutCubic);
    }
}
