using UnityEngine;
using FMODUnity;
using UnityEngine.UI;

public class GlobalVolumeCustomChange : MonoBehaviour {
    [ParamRef]
    public string parameter;

    private FMOD.Studio.PARAMETER_DESCRIPTION parameterDescription;

    private void Awake() {
        if (string.IsNullOrEmpty(parameterDescription.name))
            RuntimeManager.StudioSystem.getParameterDescriptionByName(parameter, out parameterDescription);

    }
    public void TriggerParameters(Slider value) {
        //if (!string.IsNullOrEmpty(parameter))
            //RuntimeManager.StudioSystem.setParameterByID(parameterDescription.id, value.value);
    }

}
