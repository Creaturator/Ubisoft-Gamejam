using UnityEngine;
using UnityEngine.SceneManagement;

namespace Backend {
    public class GameManager : MonoBehaviour {
        public int colorZeroAmountToWin;
        public int colorOneAmountToWin;
        public int colorTwoAmountToWin;
        public int colorThreeAmountToWin;

        public int totalColorZero;
        public int totalColorOne;
        public int totalColorTwo;
        public int totalColorThree;

        public Color color0;
        public Color color1;
        public Color color2;
        public Color color3;

        private GameObject[] ballsInLevel;

        public GameObject winButton;
        public GameObject pauseButton;
        private bool isGameWin = false;

        private void Start() {
            ballsInLevel = GameObject.FindGameObjectsWithTag("Ball");
            Debug.Log("balls total is " + ballsInLevel.Length);
        
        }

        private void Update() {
            if (!Input.GetKeyDown(KeyCode.R)) return;
            reloadScene();
        }

        public static void reloadScene() {
            Scene scene = SceneManager.GetActiveScene(); 
            SceneManager.LoadScene(scene.name, LoadSceneMode.Single);
            /*if (scene.name.ToLower().Contains("level"))
                SceneManager.LoadScene("PauseMenu", LoadSceneMode.Additive);*/
            Debug.Log("RELOAD SCENE");
        }
    
        public void CheckForWinCondition() {
            totalColorZero = 0;
            totalColorOne = 0;
            totalColorTwo = 0;
            totalColorThree = 0;
            foreach (GameObject t in ballsInLevel){
                if (t.GetComponent<BallCollision>().currentColor == color0) {
                    totalColorZero++;
                }
                else if (t.GetComponent<BallCollision>().currentColor == color1) {
                    totalColorOne++;
                }
                else if (t.GetComponent<BallCollision>().currentColor == color2) {
                    totalColorTwo++;
                }
                else if (t.GetComponent<BallCollision>().currentColor == color3) {
                    totalColorThree++;
                }
            }

            if (totalColorZero != colorZeroAmountToWin || totalColorOne != colorOneAmountToWin ||
                totalColorTwo != colorTwoAmountToWin || totalColorThree != colorThreeAmountToWin) return;

            if (!isGameWin)
            {
                isGameWin = true;
                winButton.SetActive(true);
                pauseButton.SetActive(false);
                SaveManager.addLevel(SceneManager.GetActiveScene().buildIndex);
            }
        }

        /**
     * Loads next level if it exists otherwise loads win scene
     */
        public void loadNextLevel() {
            Debug.Log("Current level " + SaveManager.currentLevel);

            Debug.Log("Incrementet " + SaveManager.currentLevel);
            if (SaveManager.currentLevel > 23) {
                Debug.Log("Last win scene");
                return;
            }
            SceneManager.LoadScene(SaveManager.currentLevel+1);
        }

    }
}
