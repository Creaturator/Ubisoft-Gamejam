using UnityEngine;

namespace Backend {
    public static class SaveManager {
        /**
     * Change range according to the maximum levels
     */
        private static int levelsUnlocked;
        public static int currentLevel = 0;

        public static void ResetLevels() {
            PlayerPrefs.SetInt("Levels", 0);
            levelsUnlocked = 0;
        }

        public static int getUnlockedLevels() => levelsUnlocked;

        public static void LoadLevels() {
            if (PlayerPrefs.HasKey("Levels")) 
                levelsUnlocked = PlayerPrefs.GetInt("Levels");
            levelsUnlocked = 24;
        }

        public static void addLevel(int currentLevel) {
            SaveManager.currentLevel = currentLevel;

            if (currentLevel < levelsUnlocked) return;

            levelsUnlocked++;
            PlayerPrefs.SetInt("Levels", levelsUnlocked);
            PlayerPrefs.Save();
        }
    }
}
