using System;
using UnityEngine;
//using FMODUnity;

public class EventEmitterVolumeControll : MonoBehaviour {
  //  private FMOD.Studio.EventInstance instance;

    //public FMODUnity.EventReference eventReference;

    [Range(0f, 1f)]
    public float volume;
    private float previousVolume;

    private void Start() {
      //  instance = FMODUnity.RuntimeManager.CreateInstance(eventReference);
        //instance.start();
    }

    private void Update() {
        if (Math.Abs(previousVolume - volume) <= 0.00001f)
            return;
        //instance.setParameterByName("Volume", volume);
        previousVolume = volume;
    }
}
