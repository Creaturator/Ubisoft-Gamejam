using UnityEngine;
using DG.Tweening;
using UI;

public class LaunchBall : MonoBehaviour {
    public Transform arrowMeshPivotTransform;
    public Transform arrowMesh;
    [SerializeField] 
    private float speed = 10;

    private Rigidbody rb;
    private bool isSelected;
    public Vector3 launchDirection = Vector3.zero;
    
    public float dragDistance;
    public bool canBeLaunched;

    private BallCollision ballCollision;

    private float dragOnPaused;
    private Vector3 velocityOnPaused = Vector3.zero;

    private void Start() {
        rb = GetComponent<Rigidbody>();
        ballCollision = GetComponent<BallCollision>();
        arrowMesh.gameObject.SetActive(false);
    }

    private void Update() => ProcessInputs();

    private void ProcessInputs() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            if (!canBeLaunched) return;
            rb.AddForceAtPosition(launchDirection * (dragDistance * speed), transform.position, ForceMode.Impulse);
            canBeLaunched = false;
            arrowMesh.gameObject.SetActive(false);
        }
        if (Input.GetMouseButton(0) && isSelected && canBeLaunched) 
            DrawLaunchLine();
    }
    
    private void OnMouseDown() => isSelected = true;
    private void OnMouseUp() => isSelected = false;

    private void OnMouseOver() {
        if (canBeLaunched == false) return;

        Color col = ballCollision.currentColor;

        ballCollision.mat.DOColor(new Color(col.r + 0.5f, col.g + 0.5f, col.b + 0.5f),
            0.2f).
            SetEase(Ease.OutCubic);
    }
    private void OnMouseExit() {
        if (canBeLaunched == false) return;

        Color col = ballCollision.currentColor;
        ballCollision.mat.DOColor(col,
            0.2f).
            SetEase(Ease.OutCubic);
    }

    private void DrawLaunchLine() {
        Vector3 rayHitPosition = Vector3.zero;

        if (Camera.main != null) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit)) {
                rayHitPosition = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            }
        }

        launchDirection =  rayHitPosition - transform.position;
        launchDirection = -launchDirection;
        dragDistance = Vector3.Distance(rayHitPosition, transform.position);
        dragDistance = Mathf.Clamp(dragDistance, 0, 10);
        
        const float multiplier = 2.5f;
        float dragArrowScale = dragDistance * multiplier;
        arrowMesh.gameObject.SetActive(true);
        arrowMeshPivotTransform.GetChild(0).transform.localScale = new Vector3( dragArrowScale, transform.localScale.y, transform.localScale.z) * 0.7f * multiplier;
        arrowMeshPivotTransform.transform.rotation = Quaternion.LookRotation(launchDirection);

    }

    public void stopOnGamePause(bool shouldPause) {
        Rigidbody body = gameObject.GetComponent<Rigidbody>();
        if (shouldPause) {
            dragOnPaused = body.drag;
            velocityOnPaused = body.velocity; //TODO: Drag is getting deleted
            body.Sleep();
        }
        else {
            body.WakeUp();
            body.drag = dragOnPaused;
            body.velocity = velocityOnPaused;
        }
    }

    private void OnDestroy() => PauseMenuToggle.onGamePaused -= stopOnGamePause;
}
