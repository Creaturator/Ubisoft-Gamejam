using Backend;
using UnityEngine;
using DG.Tweening;
//using FMODUnity;

public class BallCollision : MonoBehaviour {
    //public FMODUnity.StudioEventEmitter emitter;
    private GameManager gameManager;
    private new MeshRenderer renderer;
    public Material mat;

    public bool dontChangeColor;
    public bool changeColorOneTime = true; 

    [SerializeField][Range(0.05f,1f)] private float colorFadeDuration = 0.25f;

    public enum ColorZeroFromManager    { color0, color1, color2, color3};
    public enum ColorOneFromManager     { color0, color1, color2, color3};
    public enum ColorTwoFromManager     { color0, color1, color2, color3};
    public enum ColorThreeFromManager   { color0, color1, color2, color3 };

    public ColorZeroFromManager     colorZeroFromManager;
    public ColorOneFromManager      colorOneFromManager;
    public ColorTwoFromManager      colorTwoFromManager;
    public ColorThreeFromManager   colorThreeFromManager;


    private Color color0;
    private Color color1;
    private Color color2;
    private Color color3;

    public Color currentColor;

    public int totalColors = 3;
    public int collisionCounter;

    private void Awake() => DOTween.Init(true, true, LogBehaviour.Default);

    private void Start() {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").transform.GetComponent<GameManager>();

        renderer = transform.GetComponent<MeshRenderer>();
        mat = renderer.material;

        Init();
    }

    private void OnCollisionEnter(Collision collision) {
        gameManager.CheckForWinCondition();
        /*if(emitter != null) {
            float audioFadeInVolume = 0;
            DOTween.To(() => audioFadeInVolume, x => audioFadeInVolume = x, 1, 2);
            Debug.Log("audio volume is " + audioFadeInVolume);
            emitter.SetParameter("Volume", 1);
        }*/
        
        if (dontChangeColor) return;

        if (!collision.transform.CompareTag("Ball")) return;
        Debug.Log("Hit a ball!");

        collisionCounter++;
        collisionCounter = collisionCounter % totalColors;

        Debug.Log("Collision counter is " + collisionCounter);

        if(!changeColorOneTime) {
            switch (collisionCounter) {
                case 0:
                    currentColor = color0;
                    break;
                case 1:
                    currentColor = color1;
                    break;
                case 2:
                    currentColor = color2;
                    break;
                case 3:
                    currentColor = color3;
                    break;
            }
        }
        else
            currentColor = color1;
        
        mat.DOColor(currentColor, colorFadeDuration).SetEase(Ease.OutCubic);
    }

    private void Init() {
        //DUPLICATE CODE I KNOW BUT IT WORKS!

        switch (colorZeroFromManager) {
            //color 0
            case ColorZeroFromManager.color0:
                color0 = gameManager.color0;
                break;
            case ColorZeroFromManager.color1:
                color0 = gameManager.color1;
                break;
            case ColorZeroFromManager.color2:
                color0 = gameManager.color2;
                break;
            case ColorZeroFromManager.color3:
                color0 = gameManager.color3;
                break;
        }

        switch (colorOneFromManager)
        {
            // color 1
            case ColorOneFromManager.color0:
                color1 = gameManager.color0;
                break;
            case ColorOneFromManager.color1:
                color1 = gameManager.color1;
                break;
            case ColorOneFromManager.color2:
                color1 = gameManager.color2;
                break;
            case ColorOneFromManager.color3:
                color1 = gameManager.color3;
                break;
        }

        switch (colorTwoFromManager)
        {
            // color 2
            case ColorTwoFromManager.color0:
                color2 = gameManager.color0;
                break;
            case ColorTwoFromManager.color1:
                color2 = gameManager.color1;
                break;
            case ColorTwoFromManager.color2:
                color2 = gameManager.color2;
                break;
            case ColorTwoFromManager.color3:
                color2 = gameManager.color3;
                break;
        }

        switch (colorThreeFromManager)
        {
            // color 3
            case ColorThreeFromManager.color0:
                color2 = gameManager.color0;
                break;
            case ColorThreeFromManager.color1:
                color2 = gameManager.color1;
                break;
            case ColorThreeFromManager.color2:
                color2 = gameManager.color2;
                break;
            case ColorThreeFromManager.color3:
                color2 = gameManager.color3;
                break;
        }

        currentColor = color0;

        mat.color = color0;
    }
}
