using System.Collections.Generic;
using UI;
using UnityEngine;

public class Obstacle : MonoBehaviour {
    public bool isAnimated;
    private Rigidbody rb;
    public Vector3 startPosition;
    public Vector3 endPosition;

    public Vector3 startRotation;
    public Vector3 endRotation;

    public bool pingPongRotation = true;

    public float animSpeedPosition = 1;
    public float animSpeedRotation = 1;

    public List<Vector3> positions = new List<Vector3>();
    private Vector3[] waypoints;

    public float animTimePosition;
    public float animTimeRotation;

    private Vector3 rotationVector;
    private Quaternion rotation;

    private float dragOnPaused;
    private Vector3 velocityOnPaused = Vector3.zero;

    private void Start() {
        rb = GetComponent<Rigidbody>();
        startPosition += transform.position;
        endPosition += transform.position;
        startRotation += transform.eulerAngles;
        endRotation += transform.eulerAngles;
    }
    
    private void FixedUpdate() {
        animTimePosition = Mathf.PingPong(Time.fixedTime * animSpeedPosition, 1);

        if(pingPongRotation) {
            animTimeRotation = Mathf.PingPong(Time.fixedTime * animSpeedRotation, 1);
            rotationVector = Vector3.Lerp(startRotation, endRotation, animTimeRotation);
        }
        else {
            animTimeRotation = Time.fixedTime * (animSpeedRotation * 45);
            rotationVector = new Vector3(0, startRotation.y + animTimeRotation, 0);
        }
        
        rotation = Quaternion.Euler(rotationVector);
        rb.MovePosition(Vector3.Lerp(startPosition, endPosition, animTimePosition));
        rb.MoveRotation( rotation);
    }

    public void stopOnPause(bool shouldPause) {
        if (shouldPause) {
            dragOnPaused = rb.drag;
            velocityOnPaused = rb.velocity; //TODO: Drag is getting deleted, not tested
            rb.Sleep();
        }
        else {
            rb.drag = dragOnPaused;
            rb.velocity = velocityOnPaused;
            rb.WakeUp();
        }
    }

    private void OnDestroy() => PauseMenuToggle.onGamePaused -= stopOnPause;
}
