using UnityEngine;

[CreateAssetMenu(fileName ="DataObject", menuName ="MusicVolumes")]
public class VolumesObject : ScriptableObject {
    [Range(0,1)]
    public float musicVolume = 1;

    [Range(0,1)]
    public float vfxVolume = 1;
}
