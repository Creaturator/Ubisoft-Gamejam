using Backend;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using FMODUnity;

namespace UI
{
    public class MainUI : MonoBehaviour {
        [SerializeField]
        private VolumesObject m_MusicVolumes;

        [SerializeField]
        private Slider m_MusicValueSlider;

        [SerializeField]
        private Slider m_VFXValueSlider;

        [ParamRef]
        public string volumeParameter;
        private FMOD.Studio.PARAMETER_DESCRIPTION volumeParameterDescription;

        [ParamRef]
        public string vfxParameter;
        private FMOD.Studio.PARAMETER_DESCRIPTION vfxParameterDescription;

        private void Awake()
        {
            if (string.IsNullOrEmpty(volumeParameterDescription.name))
            {
                RuntimeManager.StudioSystem.getParameterDescriptionByName(volumeParameter, out volumeParameterDescription);
                m_MusicValueSlider.value = m_MusicVolumes.musicVolume;
                ChangeVolumeMusic();
            }

            if (string.IsNullOrEmpty(vfxParameterDescription.name))
            {
                RuntimeManager.StudioSystem.getParameterDescriptionByName(vfxParameter, out vfxParameterDescription);
                m_VFXValueSlider.value = m_MusicVolumes.vfxVolume;
                ChangeVolumeVFX();
            }
        }

        public void ChangeVolumeMusic()
        {
            if (!string.IsNullOrEmpty(volumeParameter))
            {
                m_MusicVolumes.musicVolume = m_MusicValueSlider.value;
                RuntimeManager.StudioSystem.setParameterByID(volumeParameterDescription.id, m_MusicVolumes.musicVolume);
            }
        }

        public void ChangeVolumeVFX()
        {
            if (!string.IsNullOrEmpty(vfxParameter))
            {
                m_MusicVolumes.vfxVolume = m_VFXValueSlider.value;
                RuntimeManager.StudioSystem.setParameterByID(vfxParameterDescription.id, m_MusicVolumes.vfxVolume);
            }
        }


        private void OnEnable()
        {
            SaveManager.LoadLevels();
        }

        // private void Start() => SceneManager.sceneLoaded += PauseMenuAdder.AddPauseMenuToLevel;

        public void PlayLevel(int Level) 
        {
            startLevelStatic(Level); 
        }

        public static void startLevelStatic(int Level) {
            SceneManager.LoadScene(Level, LoadSceneMode.Single);
/*            if (SceneManager.GetSceneByBuildIndex(Level).name.ToLower().Contains("level"))
                SceneManager.LoadScene("PauseMenu", LoadSceneMode.Additive);*/
            SaveManager.currentLevel = Level;
        }
    
        public void ExitApplication() {
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
    }
}
