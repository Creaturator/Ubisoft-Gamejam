using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI {
    public static class PauseMenuAdder {

        public static void AddPauseMenuToLevel(Scene newScene, LoadSceneMode mode) {
            if (newScene.name.ToLower().Contains("level")) {
                PauseMenuToggle.onGamePaused = b => { };
                foreach(LaunchBall ball in newScene.GetRootGameObjects().Where(g => g.TryGetComponent(out LaunchBall _)).Select(g => g.GetComponent<LaunchBall>()))
                    PauseMenuToggle.onGamePaused += ball.stopOnGamePause;
                foreach(Obstacle obs in newScene.GetRootGameObjects().Where(g => g.TryGetComponent(out Obstacle _)).Select(g => g.GetComponent<Obstacle>()))
                    PauseMenuToggle.onGamePaused += obs.stopOnPause;
                return;
            }
        
            if (mode != LoadSceneMode.Additive)
                return;
            if (!SceneManager.GetAllScenes().Any(s => s.name.ToLower().Contains("level") && s.isLoaded)) {
                Debug.LogError("There is no Level Loaded.");
                return;
            }

            Scene level = SceneManager.GetAllScenes().First(s => s.isLoaded && s.name.ToLower().Contains("level"));

            if (!newScene.GetRootGameObjects().Any(g => g.name.Equals("Canvas"))) {
                Debug.LogError($"In the Scene {newScene.name} is no Canvas.");
                return;
            }

            Transform canvas = newScene.GetRootGameObjects().First(g => g.name.Equals("Canvas")).transform;
            if (canvas.Find("Parent") == null)
                return;
            GameObject parent = canvas.Find("Parent").gameObject;
            GameObject button = canvas.Find("SettingsButton").gameObject;
            parent.transform.SetParent(null);
            button.transform.SetParent(null);
            SceneManager.MoveGameObjectToScene(parent, level);
            SceneManager.MoveGameObjectToScene(button, level);
            SceneManager.UnloadSceneAsync(newScene);
            canvas = level.GetRootGameObjects().First(g => g.name.Equals("Canvas")).transform;
            parent.transform.SetParent(canvas);
            button.transform.SetParent(canvas);
            Vector3 localPosition = button.transform.localPosition;
            localPosition = new Vector3(localPosition.x, canvas.transform.localPosition.y, localPosition.z);
            button.transform.localPosition = localPosition;
            parent.transform.localPosition = Vector3.zero;
            if (!parent.activeSelf) 
                return;
            Debug.LogWarning("Someone forgot to turn off the PauseMenu Parent. Doing so in Code.");
            parent.SetActive(false);
        }

    }
}
