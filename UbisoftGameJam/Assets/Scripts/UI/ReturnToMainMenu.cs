using UnityEngine;

namespace UI {
	public class ReturnToMainMenu : MonoBehaviour {
    	public void returnToMainMenu() => MainUI.startLevelStatic(0);
	}
}