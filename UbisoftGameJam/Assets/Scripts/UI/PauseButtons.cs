using Backend;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class PauseButtons : MonoBehaviour {


        public void reloadScene() => GameManager.reloadScene();

        public void loadMenu() => SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
}
