using Backend;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LevelsView : MonoBehaviour {
        [SerializeField]
        private Button[] UIButtons;

        private void OnEnable() {
            SetButtonsStatus();
        }
    
        private void SetButtonsStatus() {

            int activeButtonsCount = SaveManager.getUnlockedLevels() + 1;

            for (int ind = 0; ind < UIButtons.Length; ind++) 
                UIButtons[ind].interactable = ind < activeButtonsCount;
        }
    }
}
