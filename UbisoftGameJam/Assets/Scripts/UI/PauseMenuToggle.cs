using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class PauseMenuToggle : MonoBehaviour {

        private Image buttonImage;

        [SerializeField] 
        private Sprite closeText;

        private Sprite defaultText;

        public static Action<bool> onGamePaused;

        private void Start() {
            buttonImage = gameObject.GetComponent<Image>();
            defaultText = buttonImage.sprite;
            defaultText = buttonImage.sprite;
        }

        public void togglePauseMenu() {
            GameObject parent = gameObject.transform.parent.Find("Parent").gameObject;
            parent.SetActive(!parent.activeSelf);
            buttonImage.sprite = !parent.activeSelf ? defaultText : closeText;
            try {
                onGamePaused.Invoke(parent.activeSelf);
            }
            catch (MissingReferenceException e) {
                Debug.Log(e.Message);
            }
        }

    }
}
