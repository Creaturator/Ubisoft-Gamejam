using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TogglePause : MonoBehaviour
{
    [SerializeField]
    private GameObject[] toggleOnPauseObjects;

    [SerializeField]
    private GameObject[] toggleOffPauseObjects;
    
    private bool isPause = false;

    public void Toggle()
    {
        isPause = !isPause;

        for (int i = 0; i < toggleOnPauseObjects.Length; i++)
        {
            toggleOnPauseObjects[i].SetActive(isPause);
        }

        for (int i = 0; i < toggleOffPauseObjects.Length; i++)
        {
            toggleOffPauseObjects[i].SetActive(!isPause);
        }
    }
}
